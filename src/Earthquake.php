<?php

namespace Frhag\BmkgEarthquake;

use Carbon\Carbon;
use GuzzleHttp\Client;
use SimpleXMLElement;

/**
 * Responsible for fetching and parsing earthquake data.
 * Data is provided by Badan Meteorologi, Klimatologi, dan Geofisika aka BMKG
 * https://www.bmkg.go.id/
 * The data only consists of 60 latest earthquake with magnitude 5 or more
 * that happened in Indonesia.
 * Data is sorted by date and time in descending order.
 * 
 * @author Fredy Rhag <fredy.rhag@gmail.com>
 * @version 1.0
 */
class Earthquake
{
    /** @var string $url URL for BMKG earthquake open data */
    public static $url = 'https://data.bmkg.go.id/en_gempaterkini.xml';

    /**
     * Parse XML data to array
     *
     * Parse XML data, translate field names to english, convert time and date
     * data to Carbon object
     *
     * @param SimpleXMLElement $xml Data retrieved from request
     * @return array<int, array> $parsed
     **/
    private static function parseToArray(SimpleXMLElement $xml): array
    {
        $parsed = [];

        foreach ($xml as $node) {
            $node = (array) $node;
            $coordinates = (array) $node['point']->coordinates;

            $time = Carbon::createFromFormat('d-M-y H:i:s e', $node['Tanggal'].' '.$node['Jam']);

            $parsed[] = [
                'time'        => $time,
                'coordinates' => $coordinates[0],
                'lat'         => $node['Lintang'],
                'lon'         => $node['Bujur'],
                'magnitude'   => $node['Magnitude'],
                'depth'       => $node['Kedalaman'],
                'area'        => $node['Wilayah']
            ];
        }

        return $parsed;
    }

    /**
     * Fetch BMKG data
     *
     * Perform Guzzle request to fetch earthquake data from BMKG
     *
     * @return array<int, array>
     **/
    public static function fetch(): array
    {
        $client = new Client();
        $response = $client->get(static::$url);

        if (!$xmlString = simplexml_load_string($response->getBody())) {
            throw new \Exception('Invalid XML string from server!', 500);
        }

        return static::parseToArray($xmlString);
    }
}