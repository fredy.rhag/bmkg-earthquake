<?php

namespace Frhag\BmkgEarthquake;

use Illuminate\Support\ServiceProvider;

class BmkgEarthquakeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Earthquake::class, function($app) {
            return new Earthquake();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
    }
}
