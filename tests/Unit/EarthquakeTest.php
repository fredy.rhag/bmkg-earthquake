<?php

namespace Frhag\BmkgEarthquake\Tests;

use Frhag\BmkgEarthquake\Earthquake;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class EarthquakeTest extends TestCase
{
    /** @test */
    public function it_can_fetch_data_from_bmkg()
    {
        $earthquakeData = Earthquake::fetch();

        $this->assertNotNull($earthquakeData);

        $client = new Client();
        $response = $client->get(Earthquake::$url);
        $earthquakeGuzzle = simplexml_load_string($response->getBody());

        $this->assertEquals(count($earthquakeData), count($earthquakeGuzzle));
    }

    /** @test */
    public function it_maps_fields_correctly()
    {
        $earthquakeData = Earthquake::fetch();

        $client = new Client();
        $response = $client->get(Earthquake::$url);
        $earthquakeGuzzle = simplexml_load_string($response->getBody());
        $earthquakeGuzzle = json_decode(json_encode($earthquakeGuzzle), true);

        foreach ($earthquakeGuzzle['gempa'] as $key => $value) {
            $date = $earthquakeData[$key]['time']->format('d-M-y');
            $time = $earthquakeData[$key]['time']->format('H:i:s e');

            $this->assertEquals($value['Tanggal'], $date);
            $this->assertEquals($value['Jam'], $time);
            $this->assertEquals($value['point']['coordinates'], $earthquakeData[$key]['coordinates']);
            $this->assertEquals($value['Lintang'], $earthquakeData[$key]['lat']);
            $this->assertEquals($value['Bujur'], $earthquakeData[$key]['lon']);
            $this->assertEquals($value['Magnitude'], $earthquakeData[$key]['magnitude']);
            $this->assertEquals($value['Kedalaman'], $earthquakeData[$key]['depth']);
            $this->assertEquals($value['Wilayah'], $earthquakeData[$key]['area']);
        }
    }
}