# BMKG Earthquake

A PHP wrapper package for the BMKG earthquake open data.


Badan Meteorologi, Klimatologi, dan Geofisika a.k.a [BMKG](https://www.bmkg.go.id/) is the Indonesian official source of information related to weather, climate, earthquake etc.

This package gets information about earthquake that happened in Indonesia, and ease the integration to other PHP application. The data only consists of 60 latest earthquake with magnitude 5 or more.


This package and the author is not related to BMKG.

## Installation

Use the package manager [composer](https://getcomposer.org/) to install.

```bash
composer require frhag/bmkg-earthquake
```

## Usage
This package only have single usable static method `fetch()` of `Earthquake` class.

```php
$resultArray = Earthquake::fetch();
```
### Standalone
To try the package as standalone, create a new directory, cd in to it, and perform installation via composer

```bash
mkdir myapp
cd myapp
composer require frhag/bmkg-earthquake --prefer-dist
```

Create a file named index.php in that directory and paste this code:

```php
<?php

require 'vendor/autoload.php';

use Frhag\BmkgEarthquake\Earthquake;

echo json_encode(Earthquake::fetch());
```

Execute the file
```bash
php index.php
```

Output
```json
[{
	"time": "2020-08-02T16:44:24.000000Z",
	"coordinates": "124.41,-0.45",
	"lat": "0.45 S",
	"lon": "124.41 E",
	"magnitude": "5.0 Richter Scale",
	"depth": "10 Km",
	"area": "103 km SouthEast BOLAANGUKI-BOLSEL-SULUT"
}, {
	"time": "2020-07-30T02:51:20.000000Z",
	"coordinates": "131.11,-3.43",
	"lat": "3.43 S",
	"lon": "131.11 E",
	"magnitude": "5.4 Richter Scale",
	"depth": "15 Km",
	"area": "77 km SouthEast SERAMBAGIANTIMUR-MALUKU"
},
...
]
```

### With Laravel
Perform installation via composer in the Laravel project. Laravel version 5.5 and above will automatically discover the newly installed package.

To quickly try the package, add this code to Laravel route web.php file:
```php
...

Route::get('bmkg-earthquake', function() {
    return \Frhag\BmkgEarthquake\Earthquake::fetch();
});

...
```
Test it in browser by opening the bmkg-earthquake path.

Output
```json
[{
	"time": "2020-08-02T16:44:24.000000Z",
	"coordinates": "124.41,-0.45",
	"lat": "0.45 S",
	"lon": "124.41 E",
	"magnitude": "5.0 Richter Scale",
	"depth": "10 Km",
	"area": "103 km SouthEast BOLAANGUKI-BOLSEL-SULUT"
}, {
	"time": "2020-07-30T02:51:20.000000Z",
	"coordinates": "131.11,-3.43",
	"lat": "3.43 S",
	"lon": "131.11 E",
	"magnitude": "5.4 Richter Scale",
	"depth": "15 Km",
	"area": "77 km SouthEast SERAMBAGIANTIMUR-MALUKU"
},
...
]
```



## License
[MIT](https://gitlab.com/fredy.rhag/bmkg-earthquake/-/blob/master/LICENSE)